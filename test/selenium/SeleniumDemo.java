package test.selenium;

import java.util.concurrent.TimeUnit;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {

	public static void main(String args[]) {
		System.setProperty("webdriver.chrome.driver", "//home//kama//Pobrane//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		driver.get("https://www.allegro.pl");

		driver.findElement(By.xpath("(//a[span[text()='zaloguj']])[2]")).click();
		;
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys(Credentials.LOGIN);
		;
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(Credentials.PASSWORD);
		driver.findElement(By.xpath("//button[@id='login-button']")).click();
		try{
			Thread.sleep(1000);
			}
			catch(InterruptedException ie){
			}

		driver.findElement(By.xpath("//input[@id='main-search-text']")).sendKeys("laptop samsung");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.xpath("//a[span[text()='Komputery']]")).click();
		driver.findElement(By.xpath("//a[span[text()='Laptopy']]")).click();
		driver.findElement(By.xpath("//a[span[text()='Samsung']]")).click();
		driver.findElement(By.xpath("//a[span[text()='nowe']]")).click();

		driver.findElement(By.xpath("//button[@class='sort__sorting-list__btn__8xObu']")).click();
		driver.findElement(By.xpath("//ul[@role='menu']//li[span[text()='cena']]//a[text()='od najniższej']")).click();
		try{
			Thread.sleep(1500);
			}
			catch(InterruptedException ie){
			}
		driver.findElement(By.xpath("(//div[@class='item__item__details__1_yxy']//a[@href])[1]")).click();
		try{
			Thread.sleep(500);
			}
			catch(InterruptedException ie){
			}
		driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();		
		try{
			Thread.sleep(2000);
			}
			catch(InterruptedException ie){
			}
		driver.findElement(By.xpath("//aside[@class='step-back secondary-action']")).click();		
		try{
			Thread.sleep(1000);
			}
			catch(InterruptedException ie){
			}

		driver.findElement(By.xpath("(//a[span[text()='wyloguj']])[2]")).click();

		driver.close();

		System.out.println("Test pass");

	}
}
